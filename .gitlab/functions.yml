###############################################################################
#                           Shell functions                                   #
###############################################################################
.check_artifacts: |
  check_link() {
    curl --output /dev/null --silent --head --fail "$1"
  }
  set -x
  if [[ "${TMT_PLAN}" == "^/plans/create-osbuildvm-images$" ]]; then
    exit 0
  fi
  if [[ "${IMPORT_IMAGE}" == "yes"  ]]; then
    exit 0
  fi
  if [[ "$BUILD_FORMAT" == "img" ]]; then
    BUILD_FORMAT="raw"
  fi
  # Now we compress all teh images, so we add the .xz extension
  if [[ -n "${S3_UPLOAD_DIR}" ]]; then
    S3_UPLOAD_PREFIX="${S3_UPLOAD_DIR}"
  elif [[ "${SAMPLE_IMAGE}" == "yes" ]]; then
    S3_UPLOAD_PREFIX="sample-images"
  else
    S3_UPLOAD_PREFIX="non-sample-images"
  fi
  BASE_URL="${CI_REPOS_ENDPOINT}/${WORKSPACE_ID}/${S3_UPLOAD_PREFIX}/${IMAGE_KEY}"
  IMAGE_URL="${BASE_URL}.${BUILD_FORMAT}"

  declare -A base_names titles
  if [[ "${BUILD_FORMAT}" == "aboot" ]]; then
    echo "[+] Checking links to artifacts of aboot build in ${IMAGE_URL}"
    base_names[aboot_image]="aboot.img.xz"
    base_names[aboot_sha]="aboot.img.xz.sha256"
    base_names[rootfs_image]="rootfs.img.xz"
    base_names[rootfs_sha]="rootfs.img.xz.sha256"
    titles[aboot_image]="aboot image"
    titles[aboot_sha]="aboot checksum"
    titles[rootfs_image]="rootfs image"
    titles[rootfs_sha]="rootfs checksum"
    url="${IMAGE_URL}/"
    delim="/"
  else
    echo "[+] Checking link to ${IMAGE_URL}"
    base_names[image]=".xz"
    base_names[checksum]=".xz.sha256"
    titles[image]="Image"
    titles[checksum]="Checksum"
    url="${IMAGE_URL}"
    delim=""
  fi
  build_info=""
  for base_name in ${!base_names[@]}; do
    check_link "${url}${base_names[$base_name]}"
    build_info+="<b>Download ${titles[$base_name]}&#58;</b> "
    build_info+="<a href=${url}${base_names[$base_name]}>${IMAGE_KEY}.${BUILD_FORMAT}$delim${base_names[$base_name]}</a>"
    build_info+="<br/>"
  done
  cat <<-EOF > "build_info.html"
  $build_info
  EOF


.testing-farm-request: |
  ####################################################
  # Arguments:
  #   $1 = Full URL to images, e.g.: "${BASE_URL}"
  # Returns:
  #   Escaped json (string), e.g.: "{\"a\":\"b\"}"
  ####################################################
  testing_farm_aboot_structure() {
    BOOT_IMG=$1/aboot.img.xz
    ROOT_IMG=$1/rootfs.img.xz

    echo '{
      "boot_image": "'${BOOT_IMG}'",
      "boot_checksum": "'${BOOT_IMG}'.sha256",
      "root_image": "'${ROOT_IMG}'",
      "root_checksum": "'${ROOT_IMG}'.sha256"
     }' \
     | jq '@json "\(.)"'
  }

  # Populate physical boards specific data
  populate_board_variables() {
    case "${HW_TARGET}" in
      qdrive3)
        S3_UPLOAD_PREFIX="QDrive3"
        eval TF_POOL_"${ARCH}"="qdrive3-direct"
        ;;
      ridesx4)
        S3_UPLOAD_PREFIX="RideSX4"
        eval TF_POOL_"${ARCH}"="ride4-ci"
        ;;
      *)
        echo "Not supported board. Using the default values"
        ;;
    esac
  }

  # In Testing Farm the aarch64 composes have the '-aarch64' appended to the end.
  # It'll be removed in the future and we won't need to do this.
  if [[ "${ARCH}" == "aarch64" ]] && [[ "${STREAM}" == "upstream" ]]; then
      TF_COMPOSE="${TF_COMPOSE}-aarch64"
  fi

  if [[ "${ACTION}" == "TEST" ]]; then
    if [[ "${BUILD_FORMAT}" == "aboot" ]]; then
      populate_board_variables
      if [[ -z "${RELEASE_NAME}" ]]; then
        # Use by the pipelines-as-code pipeline
        export BASE_URL="${WEBSERVER_WORKSPACES}/${WORKSPACE_ID}/${S3_UPLOAD_PREFIX}/${IMAGE_KEY}.aboot"
      else
        # Use by the package-level-pipeline. The IMAGE_KEY comes already with the .boot
        export BASE_URL="${WEBSERVER_RELEASES}/${RELEASE_NAME}/${S3_UPLOAD_PREFIX}/${IMAGE_KEY}"
      fi
      TF_COMPOSE=$(testing_farm_aboot_structure "${BASE_URL}" | sed 's/^"//' | sed 's/"$//')
    else
      TF_COMPOSE=${IMAGE_KEY}
    fi
  fi

  # This bash magic is to handle arch specific tf pools
  TF_POOL_VAR="TF_POOL_${ARCH}"

  # If this variables is defined is passed as extra list of variables
  # if not, it just put and extra empty var
  # NOTE: The content of the variable have to be a valid list of items for json.
  # Example:
  # META_VARIABLE="{\"API_NAME\": \"fopen\", \"AUTOQE_TEST_LIST\": \"/SourceUnitTests/fopen/\"}"
  if [[ -v META_VARIABLE ]]; then
    META_VARIABLE="$(echo $META_VARIABLE | tr -d '}' | tr -d '{' )"
  else
    META_VARIABLE="${META_VARIABLE:-\"META_VARIABLE\": \"\"}"
  fi

  # The META_CONTEXT should be a JSON format string
  # If the variable exist, it'll be used as tmt context,
  # if not, a empty JSON object will be passed
  if [[ ! -v META_CONTEXT ]]; then
    META_CONTEXT="{}"
  fi

  # The META_ENVIRONMENT should be a JSON format string
  # If the variable exist, it'll be used as tmt context,
  # if not, a empty JSON object will be passed
  if [[ ! -v META_ENVIRONMENT ]]; then
    META_ENVIRONMENT="{}"
  fi

  # Compose the query for calling the Testing Farm API
  cat <<EOF > request.json
  {
    "api_key": "${TF_API_KEY}",
    "test": {
      "tmt": {
      "url": "${CI_REPO_URL}",
      "ref": "${CI_REF}",
      "name": "${TMT_PLAN}"
      }
    },
    "environments": [
     {
     "arch": "${ARCH}",
     "os": {"compose": "${TF_COMPOSE}"},
     "pool": "${!TF_POOL_VAR}",
     "tmt": {
       "context": ${META_CONTEXT},
       "environment": ${META_ENVIRONMENT}
     },
     "variables": {
       "ARCH": "${ARCH}",
       "UUID": "${WORKSPACE_ID}",
       "REPO_URL": "${REPO_URL}",
       "REVISION": "${REVISION}",
       "CUSTOM_IMAGES_REPO": "${CUSTOM_IMAGES_REPO}",
       "CUSTOM_IMAGES_REF": "${CUSTOM_IMAGES_REF}",
       "OS_VERSION": "${OS_VERSION}",
       "OS_PREFIX": "${OS_PREFIX}",
       "OS_OPTIONS": "${BUILD_OS_OPTIONS}",
       "BUILD_TYPE": "${BUILD_TYPE}",
       "IMAGE_NAME": "${IMAGE_NAME}",
       "IMAGE_TYPE": "${IMAGE_TYPE}",
       "IMAGE_KEY": "${IMAGE_KEY}",
       "BUILD_FORMAT": "${BUILD_FORMAT}",
       "SAMPLE_IMAGE": "${SAMPLE_IMAGE}",
       "TEST_IMAGE": "${TEST_IMAGE}",
       "IMPORT_IMAGE": "${IMPORT_IMAGE}",
       "SET_IMAGE_SIZE": "${SET_IMAGE_SIZE}",
       "IMAGE_SIZE": "${IMAGE_SIZE}",
       "DISTRO_NAME": "${TEST_DISTRO_NAME}",
       "S3_BUCKET_NAME": "${S3_BUCKET_NAME}",
       "BUILD_TARGET": "${BUILD_TARGET}",
       "SSH_KEY": "${SSH_KEY}",
       "SSL_VERIFY": "${SSL_VERIFY}",
       "AWS_REGION": "${AWS_DEFAULT_REGION}",
       "AWS_TF_REGION": "${AWS_TF_REGION}",
       "AWS_CKI_REGION": "${AWS_CKI_REGION}",
       "CI_REPOS_ENDPOINT": "${CI_REPOS_ENDPOINT}",
       "PRODUCT_REPOS_ENDPOINT": "${PRODUCT_REPOS_ENDPOINT}",
       "S3_UPLOAD_DIR": "${S3_UPLOAD_DIR}",
       "STREAM": "${STREAM}",
       "PACKAGE_SET": "${PACKAGE_SET}",
       "PRODUCT_BUILD_PREFIX": "${PRODUCT_BUILD_PREFIX}",
       "PIPELINE_REPO": "${PIPELINE_REPO}",
       "SAMPLE_IMAGES_REPO": "${SAMPLE_IMAGES_REPO}",
       "SAMPLE_IMAGES_REF": "${SAMPLE_IMAGES_REF}",
       "OSBUILD_VERSION": "${OSBUILD_VERSION}",
       "ARTIFACTS_DATA_URL": "${ARTIFACTS_DATA_URL}",
       "KERNEL_NVR": "${KERNEL_NVR}",
       "ARTIFACTORY_REPO": "${ARTIFACTORY_REPO}",
       "PACKAGE_NAME": "${PACKAGE_NAME}",
       "PACKAGE_NVR": "${PACKAGE_NVR}",
       "CHILD_PACKAGE_NAME": "${CHILD_PACKAGE_NAME}",
       "CHILD_PACKAGE_NVR": "${CHILD_PACKAGE_NVR}",
       "PARENT_DOWNLOAD_URL": "${PARENT_DOWNLOAD_URL}",
       "DEPENDENCY_TYPE": "${DEPENDENCY_TYPE}",
       "RELEASE": "${RELEASE}",
       "RELEASE_NAME": "${RELEASE_NAME:-nightly}",
       "COMPOSE_NAME": "${COMPOSE_NAME}",
       "COMPOSE_DISTRO": "${COMPOSE_DISTRO}",
       "DOWNSTREAM_COMPOSE_URL": "${DOWNSTREAM_COMPOSE_URL}",
       ${META_VARIABLE}
       },
     "secrets": {
       "AWS_ACCESS_KEY_ID": "${AWS_ACCESS_KEY_ID}",
       "AWS_SECRET_ACCESS_KEY": "${AWS_SECRET_ACCESS_KEY}",
       "AWS_TF_ACCOUNT_ID": "${AWS_TF_ACCOUNT_ID}",
       "AWS_CKI_ACCOUNT_ID": "${AWS_CKI_ACCOUNT_ID}",
       "ARTIFACTORY_TOKEN": "${ARTIFACTORY_TOKEN}",
       "ARTIFACTORY_URL": "${ARTIFACTORY_URL}"
       }
     }
     ]
  }
  EOF

  # Do the API query to Testing Farm with the data from above
  curl --silent ${TF_ENDPOINT} \
      --header "Content-Type: application/json" \
      --data @request.json \
      --output response.json

  # Show the response, but hide the secrets values
  jq 'del(.environments |. [] | .secrets)' response.json

  ID=$(jq -r '.id' response.json)
  set +x
  echo "Job running at: (${TF_ENDPOINT}/${ID})"

  while true; do
      rm -f response.json
      curl --silent --output response.json "${TF_ENDPOINT}/${ID}"
      STATUS=$(jq -r '.state' response.json)
      if [[ "$STATUS" == "complete" ]] || [[ "$STATUS" == "error" ]]; then
          echo ; echo "Finished"
          break
      fi
      echo -n "."
      sleep 30
  done

  # Debug response.json
  # Removed the section xunit as it can be huge for some tests and
  # its content is already stored in the artifacts
  jq 'del(.result | .xunit)' response.json

  # TODO: delete this after debugging is no longer needed (tf bug fixed)
  # Testing-farm sometimes responds stating the job failed due to an unknown
  # error. This seems to be transient. Retry several times on this event to
  # gather debugging data.
  if [ "${STATUS}" == "error" ]; then
    echo "Refreshing testing-farm response..."
    retry_counter=10
    while (( --retry_counter >= 0 )); do
      rm -f response.json
      curl --silent --output response.json "${TF_ENDPOINT}/${ID}"
      STATUS=$(jq -r '.state' response.json)
      echo "STATUS=${STATUS}"
      if [ "$STATUS" == "complete" ]; then
          echo ; echo "Testing-farm response changed to: complete"
          break
      fi
      sleep 3
    done
  fi

  # Check the tests result
  RESULT=$(jq -r '.result.overall' response.json)
  echo "Result: $RESULT"

  # Even in failure, there are artifacts and a pipeline.log
  ARITFACTS_URL=$(jq -r '.run.artifacts //""' response.json)

  # Show the artifacts URL in the log
  if [[ -n "$ARITFACTS_URL" ]]; then
      echo "Testing Farm artifacts: $ARITFACTS_URL"
      echo "Testing Farm pipeline log: $ARITFACTS_URL/pipeline.log"
      if [[ "${ACTION}" == "TEST" ]]; then
          sleep 30
          export TESTS_RESULTS_DIR="tests_results"
          mkdir $TESTS_RESULTS_DIR || true
          # Sanitize the JOB NAME to remove characters like / [ and ]
          # It also remove the initial /plans/, to make it shorter and more meaninful
          JOB_NAME=$(echo ${CI_JOB_NAME/\/plans\//} | tr -d '[:blank:]|\[|\]' | tr -s '/' '-')
          curl --silent --output "${TESTS_RESULTS_DIR}/${JOB_NAME}-results-junit.xml" "${ARITFACTS_URL}/results-junit.xml"
      fi
  fi

  # If the result is an error, there is no report to show and means that something
  # was wrong with the call or there was an internal error from Testing Farm.
  # It does not mean that the tests failed.
  if [[ "$RESULT" == "error" ]] || [[ "$RESULT" == "unknown" ]] || [[ "$RESULT" == "skipped" ]]; then
      echo "Testing Farm has returned an error:"
      jq -r '.result.summary' response.json
      exit 10
  fi
