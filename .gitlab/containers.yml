# Containers.yml is a generic set of steps for building containers in component repositories
# The steps defined here can be automatically pulled into a component repository to
# build and push containers within it, by including the following snippet
# in the component repository:
#
# ```
#include:
# - project: "${CI_PROJECT_NAMESPACE}/pipelines-as-code"
#   file:
#     - '/.gitlab/containers.yml'
# ```
# and adding the following to the stages of the gitlab file, to invoke the steps from this file
# ```
# stages:
#  - build
# ```
#
# The newly built container image will inherit the name of the repository, for example:
# if the component repo is called my-awesome-component, then the resulting image will be as follows:
# `quay.io/automotive-toolchain/my-awesome-component:latest`.
# The container registry and namespace can be overriden with the $CI_REGISTRY variable
#
# This .yml file expects a Containerfile called `Containerfile` in the root directory of the repository
# in order to build the image.
default:
  tags: [ docker ]

.container-build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [ "" ]
  variables:
    CONTAINER_TAG: "${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME: "${CI_PROJECT_NAME}"
    CONTEXT: "${CI_PROJECT_DIR}"
    # to pass custom Containerfile path if it's different than $CONTEXT/Containerfile
    CONTAINERFILE: "${CONTEXT}/Containerfile"
    QUAY_EXPIRES_AFTER: "never"
  script:
    - echo "{\"auths\":{\"${CONTAINER_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CONTAINER_REGISTRY_USER}" "${CONTAINER_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor
      --ignore-path=/usr/bin/newuidmap
      --ignore-path=/usr/bin/newgidmap
      --ignore-path=/usr/sbin/suexec
      --label "quay.expires-after=${QUAY_EXPIRES_AFTER:-never}"
      --context="${CONTEXT}"
      --dockerfile="${CONTAINERFILE}"
      --destination="${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/${IMAGE_NAME}:${CONTAINER_TAG}"
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
      variables:
        QUAY_EXPIRES_AFTER: "1w"
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: always
      variables:
        CONTAINER_TAG: "latest"

container-build:
   extends: .container-build
